import Product from './pages/product/Product';
import './styles/index.scss'
import {Helmet} from "react-helmet"

function App() {
  return (
    <main id='main'>
      <Helmet>
        <title>Zod.TV</title>
        <meta 
          name='description'
          content='Zero Trust - Decentralized - Video Transcoding'
        />
      </Helmet>
      <Product/>
    </main>
    
  );
}

export default App;
