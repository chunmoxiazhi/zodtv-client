export const DUMMYBLOG = [
  {
    id: 0,
    image: 'near_blog1.jpg',
    title: 'Integrating with NEAR',
    date: 'DEC 21, 2020',
    note: 'A write up on our NEAR integration journey.',
    path: 'https://vans554.medium.com/integrating-with-near-protocol-96ae36263ae5'
  },
  {
    id: 1,
    image: 'ezgif-4-76cbd1b17b76.gif',
    title: 'LivePeer VS Zod.TV',
    date: 'JUN 9, 2021',
    note: 'Video will soon make up over 82% of global internet traffic.',
    path: 'https://vans554.medium.com/livepeer-zod-tv-802680a1f975'
  },
  {
    id: 2,
    image: 'mining_apocalyse.jpeg',
    title: 'The Mining Apocalypse',
    date: 'JUN 10, 2021',
    note: 'Where do all the GPUs go after Eth2.0?',
    path: 'https://zod.tv/'
  }
]
